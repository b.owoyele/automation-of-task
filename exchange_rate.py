import forex_python
from datetime import datetime 
import json
import boto3

from forex_python.converter import CurrencyRates
Hold = CurrencvRates()
response = Hold.get_rates('USD') #Current exchange rate data
#print (response)


try:
    this_moment= datetime.today().strftime('%Y_%m_%d_%H')
    file_name= f'/home/ec2-user/project2/exchange_rate_{this_moment}.json'

    with open(file_name, 'w') as json_file:
        json.dump(response, json_file)
    json_file.close()
    print(file_name)
    s3_name = 'project2/Exchange_rate_data_', this_moment,'.json'
    s3remotefile= f'exchangerate/exchange_rate_{this_moment}.json'
    s3 = boto3.client('s3', region_name= 'us-east-1')
    response = s3.upload_file(file_name, 'cloudrealityproject2bucket', s3remotefile)

except Exception as e:
# Publish Topic
    sns_client = boto3.client('sns', region_name= 'us-east-1')
    sns_client.publish (
        TopicArn = "arn:aws:sns:us-east-1:027940810740:Project2",
        Message = "Error in Foreign Exchange Processing.",
        Subject = 'Foreign Exchange Cron Job'
        )  
        