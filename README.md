## Objective:

1. Get real time exchange rates.
2. Export the exchange rate as a JSON output into an S3 bucket every hour.
3. Trigger an SNS notification in the event of a failure or error in the automated task.

## Architecture: 

![Project2](https://gitlab.com/b.owoyele/images/-/raw/main/Project2.jpg)

## Solution: 

#### Initial Setup

1. Launch an EC2 instance in the AWS console
   * Make sure that you download and save the keypair file in a safe place
2. Create a S3 bucket
3. Create and name a file in the S3 bucket
4. Create an SNS topic and add and email subscriber
   * Make sure the email subscriber confirms the subscription
5. Create and attach a role to your EC2 instance with the following permissions (remembering the rule of Least Privilege):
   * Write into S3
   * Publish an SNS topic 

#### Solution Steps: 

 1. SSH into your EC2 instance via CLI from your remote computer using the following commands:
    1. chmod 400 keyfile.pem
    2. ssh -i keyfile.pem username@publicIPaddress
       * ssh: SSH protocol command 
       * -i: specifies an alternate identification file to use for public key authentication
       * username: instance username
       * publicIPaddress: instance public IP address

         <details>
         <summary>Image of successful SSH </summary>

         ![SSH](https://gitlab.com/b.owoyele/images/-/raw/main/SSH.png) 

         </details>
 2. Create a folder in your instance using the command **mk dir foldername**
 3. Open the previously created folder using the command **cd foldername** 
 4. Within that folder create and name a python file using the command **touch** [**filename.py**](http://filename.py)
 5. Open the python file using the command **vim** [**filename.py **](http://filename.py)and press **i** to edit the file 
 6. Enter your code int he previously opened python file so that the code performs the following 
    * Run the exchange conversions 
    * Turn the exchange results into a JSON doc 
    * Name the JSON doc based on the date and time of the results on the hour (so that each new hourly result doesn't overwrite the previous one when being stored).
    * Dump the document into the appropriate S3 bucket 
    * Triggers an SNS notification in the event of a error in the previous steps 
 7. Save your code and return to the terminal by performing the following **press esc and type :wq**
 8. Test your code before automating the task by using the  command **python3** [**nameofpythonfile.py**](http://nameofpythonfile.py)
 9. In the CLI perform the following to ensure the code is running to satisfaction:
    * Go to the directory where the python script is located using the command **cd filename**
    * List the items within that directory using the command **ls**
    * In the directory you should see a json document (as a result of the python code previously run) as well as your python script

      <details>
      <summary>Example</summary>

      ![image](https://gitlab.com/b.owoyele/images/-/raw/main/Jsondoc.png)

      </details>
10. In the AWS console perform the following to ensure the code is running to satisfaction:
    * Go to and open the S3 bucket 
    * Open the folder in the S3 bucket where the JSON documents are being stored 
    * You  should see the JSON document stored and named as written in your code
    * Download and open the JSON document to ensure that is is the exchange rate conversion

      <details>
      <summary>Examples </summary>
      Example of S3 bucket

      ![image](https://gitlab.com/b.owoyele/images/-/raw/main/S3_bucket.png)

      Example of document contents 

      ![image](https://gitlab.com/b.owoyele/images/-/raw/main/DocContents.png)

      </details>
11. To ensure that you will receive and SNS notification in the event of an error in the task: 
    * Comment out a portion of code in the python script and save 
    * Return to the CLI and run the python script
    * You should receive an SNS notification notifying of the error

#### Automation of Task 

1. Return to the CLI and run the command **crontab -e** 
2. Press **i** to edit the document 
3. Enter your desired cron schedule in the following format:
   * \*\*\*\*\*/ path to python /path to python script 
4. Save and return to the terminal by performing the following **press esc and type :wq**

   <details>
   <summary>Example </summary>

   ![image](https://gitlab.com/b.owoyele/images/-/raw/main/Automation.png)

   </details>
5. To Ensure that your task is automated you can either:
   1. Change the cron schedule to every minute to see if the JSON files are being dumped in your S3 bucket ever minute 
   2. Keep your cron schedule as is and check you S3 bucket on the hour to see if a JSON file has been dumped 

### 

### 
